Raw model coefficients:
            estimate standard.error lower.CI upper.CI p.value
(Intercept)   0.8556         0.0706   0.7173   0.9940  0.0000
rrr          -0.1436         0.0934  -0.3266   0.0394  0.1241
sss          -0.1738         0.1059  -0.3813   0.0337  0.1006

***********************

Transformed coefficients:
            estimate standard.error lower.CI upper.CI p.value
(Intercept)   0.8556         0.0706   0.7173   0.9940  0.0000
amp           0.2255         0.1010   0.0275   0.4234  0.0256
acr           0.8804         0.4375   0.0230   1.7378  0.0442
            F df1 df2         p
[1,] 2.530442   2   4 0.1948852
