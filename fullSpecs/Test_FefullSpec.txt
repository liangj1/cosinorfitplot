Raw model coefficients:
            estimate standard.error lower.CI upper.CI p.value
(Intercept)   0.0000              0   0.0000   0.0000  0.0443
rrr           0.8100              0   0.8100   0.8100  0.0000
sss           0.4677              0   0.4677   0.4677  0.0000

***********************

Transformed coefficients:
            estimate standard.error lower.CI upper.CI p.value
(Intercept)   0.0000              0   0.0000   0.0000  0.0443
amp           0.9353              0   0.9353   0.9353  0.0000
acr           0.5236              0   0.5236   0.5236  0.0000
                F df1 df2            p
[1,] 1.356284e+32   2   4 2.174497e-64
