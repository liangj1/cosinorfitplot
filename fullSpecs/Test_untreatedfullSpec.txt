Raw model coefficients:
            estimate standard.error lower.CI upper.CI p.value
(Intercept)   0.0000              0   0.0000   0.0000  0.6418
rrr           0.0000              0   0.0000   0.0000  0.0536
sss           0.9353              0   0.9353   0.9353  0.0000

***********************

Transformed coefficients:
            estimate standard.error lower.CI upper.CI p.value
(Intercept)   0.0000              0   0.0000   0.0000  0.6418
amp           0.9353              0   0.9353   0.9353  0.0000
acr           1.5708              0   1.5708   1.5708  0.0000
                F df1 df2            p
[1,] 4.603603e+31   2   4 1.887402e-63
