Raw model coefficients:
            estimate standard.error lower.CI upper.CI p.value
(Intercept)   1.2165         0.1402   0.9416   1.4913  0.0000
rrr           0.0361         0.1855  -0.3275   0.3998  0.8456
sss          -0.2067         0.2104  -0.6190   0.2056  0.3257

***********************

Transformed coefficients:
            estimate standard.error lower.CI upper.CI p.value
(Intercept)   1.2165         0.1402   0.9416   1.4913  0.0000
amp           0.2099         0.2097  -0.2011   0.6208  0.3169
acr          -1.3977         0.8877  -3.1377   0.3422  0.1154
             F df1 df2         p
[1,] 0.5018655   2   4 0.6390459
