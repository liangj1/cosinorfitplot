#test cosinor
library('cosinor2')
library('ggplot2')
library('svglite')
#generate the filenames
proteinName = c('Test','Bmal1','Clock','Cry1','Irp2','Per2')
treatment = c('untreated', 'Fe', 'DP')
namecomb = expand.grid(proteinName,treatment)
csvdir = 'csv'
filenames = paste(namecomb$Var1, '_', namecomb$Var2, sep = "")


# function for plot the figures and save to pdf
rhythmplot = function(cosinorfitdata, df, linecolor, savename, time, mean)
  # cosinorfitdata has time and protein
  # df has time and protein
  # linecolor is green for Fe, red for Fe, black for Fe, need to change every time, and also change the treatment 
  # savename is paste(proteinName, treatment, '.pdf', sep='')
{
  ggplot(data=cosinorfitdata, aes(time,protein)) +
    geom_line() +
    geom_line(data = df, color=linecolor, linetype = 2,size=0.8) +
    geom_point(data = df, color=linecolor, size=3) +
    geom_errorbar(data = df, aes(x = time, ymin = protein-tailedsd, ymax = protein+tailedsd), width = 0.3, position = position_dodge(0)) +
    theme_bw() +
    labs(title="protein rhythmicity", x="time(h)", y = "relative protein abundancy") +
    theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
    # ylim(0,2) +
    annotate("point", x = time[1], y = mean[1], colour = "blue", size = 3)
  ggsave(paste(savename,'.pdf',sep = ''))
}


acroconvert = function(arctan) # convert phase in pi to 24 hour
{
  converted = (arctan)/pi * 12 # arctan/pi*12: 24 hour phase
  return(converted)
}

estiY = function(plot, estiX) # find the Y value of the estimated acrophase to find peak
{
  index = which.min(abs(plot$data$time - estiX))
  Y = plot$data$Y.hat[index]
  return(Y)
}

for (i in filenames)
{
color = 'red' # plot color
filename = paste(i,'.csv',sep='') # read in the csv for each filename
path = paste(csvdir,'/',filename,sep='')
data = read.csv(path, header=FALSE, row.names = 1)
# remove the -2 timepoint for the plot and cosinor analysis
time = as.numeric(data['time',])
tailedtime = tail(time,-1)
mean = as.numeric(data['mean',])
tailedmean = tail(mean,-1)
sd = as.numeric(data['stdev',])
tailedsd = tail(sd,-1)
# summarize the dataframe
df = data.frame(tailedtime,tailedmean)
colnames(df) = c('time','protein')
fit <- cosinor.lm(protein ~ time(time), data = df, period = 24)
# calculate the real acrophase
estiacro = summary(fit)$transformed.table[c('acr'),]$estimate #the original acrophase, closest peak/trough to (0,0)
estiacro1 = 0
if (estiacro < 0) #if smaller than 0, find next point
{estiacro1 = acroconvert(estiacro)+ 12} else
{estiacro1 = acroconvert(estiacro)}
estiacro2 = estiacro1 + 12
cosinorplot = ggplot.cosinor.lm(fit)
estiacroY1 = estiY(cosinorplot,estiacro1)
estiacroY2 = estiY(cosinorplot,estiacro2)

acro = 0 #initialize acrophase, use the peak acrophase as real acrophase
if (max(estiacroY1,estiacroY2) == estiacroY1)
{acro = estiacro1} else {acro = estiacro2}
# save fullSpecs to txt
fullSpecName = paste('fullSpecs/',i,'fullSpec.txt',sep='') #make sure that this is changed for each run
sink(fullSpecName)
print(summary(fit))
print(cosinor.detect(fit))
sink()
# save converted phase to txt
sumtxt = summary(fit)
sumamp = sumtxt$transformed.table[c('amp'),]
sumacr = sumtxt$transformed.table[c('acr'),]


fitsummaryfilename = paste('fitSummary/',i,'summary','.txt',sep='') #make sure that this is changed for each run
ampacr = rbind(sumamp, sumacr)
ampacr = subset(ampacr, select = -c(standard.error,p.value))
write.table(ampacr,file = fitsummaryfilename)
pval = cosinor.detect(fit)
write.table(pval, file = fitsummaryfilename, append = TRUE)
write.table(acro, file = fitsummaryfilename, append = TRUE)

# plot the figure
cosinorplot = ggplot.cosinor.lm(fit)
cosinorfitdata = data.frame(cosinorplot$data$time,cosinorplot$data$Y.hat)
colnames(cosinorfitdata) = c('time','protein')
pdfname = paste('plots/',i,'cosinorPlot')
rhythmplot(cosinorfitdata = cosinorfitdata, df = df,linecolor = color, savename = pdfname, time = time, mean = mean)
}

